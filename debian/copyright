Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: picom
Upstream-Contact: Yuxuan Shui <yshuiv7@gmail.com>
Source: https://github.com/yshui/picom

Files: *
Copyright: 2003 Carl Worth <cworth@cworth.org>
           2003-2005 Eric Anholt <anholt@freebsd.org>
           2003-2004 Keith Packard <keithp@keithp.com>
           2003 Matthew Allum <breakfast@10.am>
           2004 Phil Blundell <pb@reciva.com>
	   2016-2019 Yuxuan Shui <yshuiv7@gmail.com
License: Expat and MPL-2.0
Comment:
 Based on xcompmgr, originally written by Keith Packard, with modifications
 from several contributors (according to the xcompmgr man page): Matthew Allum,
 Eric Anholt, Dan Doel, Thomas Luebking, Matthew Hawn, Ely Levy, Phil Blundell,
 and Carl Worth. Menu transparency was implemented by Dana Jansens.
 .
 Numerous contributions to picom from Richard Grenville.
 .
 See the CONTRIBUTORS file for a complete list of contributors

Files: debian/*
Copyright: 2012-2016 Scott Leggett <scott@sl.id.au>
           2019-2020 Fritz Reichwald <reichwald@b1-systems.de>
           2019-2020 Nikos Tsipinakis <nikos@tsipinakis.com>
License: Expat

Files: src/backend/driver.c
       src/backend/driver.h
       src/backend/gl/glx.h
       src/backend/gl/gl_common.c
       src/backend/gl/gl_common.h
       src/backend/xrender/xrender.c
       src/backend/backend.c
       src/backend/backend.h
       src/backend/backend_common.c
       src/backend/backend_common.h
       src/compiler.h
       src/diagnostic.c
       src/diagnostic.h
       src/err.h
       src/event.c
       src/event.h
       src/kernel.c
       src/kernel.h
       src/log.h
       src/meta.h
       src/options.c
       src/options.h
       src/region.h
       src/render.c
       src/render.h
       src/string_utils.c
       src/string_utils.h
       src/types.h
       src/utils.h
       src/vsync.c
       src/vsync.h
       src/x.c
       src/x.h
Copyright: 2016-2019 Yuxuan Shui <yshuiv7@gmail.com>
License: MPL-2.0

Files: src/backend/gl/glx.c
       src/c2.c
       src/c2.h
       src/common.h
       src/config.c
       src/config.h
       src/config_libconfig.c
       src/dbus.c
       src/dbus.h
       src/opengl.c
       src/opengl.h
       src/picom.c
       src/picom.h
       src/win.c
       src/win.h
       src/xrescheck.c
       src/xrescheck.h
       subprojects/test.h/test.h
Copyright: 2003 Keith Packard <keithp@keithp.com
           2011-2013 Christopher Jeffrey <chjjeffrey@gmail.com>
           2012-2014 Richard Grenville <pyxlcy@gmail.com>
           2016-2019 Yuxuan Shui <yshuiv7@gmail.com>
License: Expat

Files: bin/picom-trans
Copyright: 2011-2012 Christopher Jeffrey <chjjeffrey@gmail.com>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: MPL-2.0
 On Debian systems the full text of the MPL-2.0 can be found in
 /usr/share/common-licenses/MPL-2.0.
